# 250 plots field survey

- [Issue tracker](https://gitlab.com/pathfinder_project/field_survey/-/issues)
- [Arena Mobile manual](https://gitlab.com/pathfinder_project/field_survey/-/wikis/Arena-mobile-instructions)
- [Source code repository](https://gitlab.com/pathfinder_project/field_survey/-/tree/main)